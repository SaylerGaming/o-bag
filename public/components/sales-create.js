import { http } from "/components/axios.js";
import SearchProduct from "/components/search-product.js";

export default {
    name: "SalesCreate",
    components: { SearchProduct },
    template: `
        <form @submit.prevent="save">
            <div class="row">
                <div class="col-md-3">
                    <input v-model="sale.show"  class="form-check-input" type="checkbox" id="publishedCheckbox" value="option1">
                    <label class="form-check-label" for="publishedCheckbox">Показывать</label>
                </div>
                <div class="col-md-3">
                    <input  v-model="sale.is_main"  class="form-check-input" type="checkbox" id="inMainPageCheckbox" value="option2">
                    <label class="form-check-label" for="inMainPageCheckbox">На главной</label>
                </div>
            </div>

            <div class="form-group">
                <label   for="exampleInputEmail1">Называние</label>
                <input v-model="sale.title"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Картинка на главной</label>
                <input  @change="mainImageHanlder"  type="file" accept=".jpg, .jpeg, .png" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
               <!-- Preview Main Image start -->
               <div v-if="sale.preview_image" class="image-preview">
                <!-- <button class="btn btn-danger btn-delete-float">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button> -->
                <img :src="sale.preview_image" >
            </div>
             <!-- Main preview END -->

            <div class="form-group">
                <label    for="exampleInputEmail1">SEO title</label>
                <input v-model="sale.seo_title"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div class="form-group">
                <label    for="exampleInputEmail1">SEO description</label>
                <input v-model="sale.seo_description"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div class="form-group">
                <label    for="exampleInputEmail1">SEO content</label>
                <input v-model="sale.seo_content"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>



            <hr/>

            <div class="additional-fields-wrapper">
                <div class="form-group">
                    <label for="exampleInputEmail1">Изображение</label>
                    <input @change="initialImageHanlder"  type="file" accept=".jpg, .jpeg, .png" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
                </div>

                <!-- Preview  Image start -->
                <div v-if="sale.image" class="image-preview">
                    <img :src="sale.image">
                </div>
                <!--  preview END -->

                <div class="form-group">
                    <label for="exampleInputEmail1">Описание</label>
                    <quill-editor v-model="sale.text" required style="max-height:300px; overflow: auto;"  :options="editorOption">
                </div>
            </div>

            <div class="additional-fields-wrapper"  v-for="(additionalField, index) in additionalFields" :key="additionalField.uuid" :id="additionalField.uuid">
                <button class="btn btn-danger btn-delete-float" @click.prevent="deleteAdditionalField(index, additionalField)">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
                <div  class="images-group" v-for="(imagee,indexx) in additionalField.image" :key="index">
                    <div class="form-group" :data-index="index">
                        <label for="exampleInputEmail1">Изображение</label>
                        <input @change="additionalImageHanlder" :data-index="indexx" type="file" accept=".jpg, .jpeg, .png" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
                    </div>
                    <!-- Preview  Image start -->
                    <div v-if="imagee" class="image-preview">
                        <img :src="imagee.instance">
                    </div>
                </div>
                <button type="submit" class="btn btn-success" @click.prevent="addImagee(additionalField)">Добавить новую картинку</button>
                <!--  preview END -->

                <div class="form-group">
                    <label for="exampleInputEmail1">Описание</label>
                    <quill-editor v-model="additionalField.text" style="max-height:300px; overflow: auto;"  :options="editorOption">
                </div>
            </div>

            <hr/>
            <!-- Products -->
            <div v-if="selectedProducts" class="included-products">
                <p v-for="(product, index) in selectedProducts">
                      <span class="label label-success">
                        #id: {{product.id}}
                        {{product.title}}
                    </span>
                     <button  class="btn btn-danger" @click.prevent="deleteProduct(product.id)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </p>


            </div>
            <search-product v-if="!isLoading" @onSelectedItems="setProducts" :items="selectedProducts"/>
            <!-- <search-product v-if="isUpdate && !isLoading"  @onSelectedItems="setProducts" :items="selectedProducts"/> -->
            <!-- End products  -->

            <hr/>

            <button type="submit" class="btn btn-success" @click.prevent="addField">Добавить поле</button>
            <button type="submit" class="btn "  :class="[isLoading ? 'btn-warning'  : 'btn-primary']" :disabled="isLoading" >Сохранить</button>
            <div v-if="responseMessage.error" class="alert alert-danger" role="alert">{{responseMessage.error}}</div>
            <div v-if="responseMessage.success" class="alert alert-success" role="alert">{{responseMessage.success}}</div>
        </form>
    `,

    data() {
        return {
            editorOption: {
                theme: "snow",
            },
            sale: {
                title: "",
                show: false,
                is_main: false,
                preview_image: "",
                text: "",
                image: "",
                seo_title:'no title',
                seo_content:'no content',
                seo_description:'no description'
            },
            selectedProducts: [],
            additionalFields: [],
            isLoading: false,
            responseMessage: {
                error: "",
                success: "",
            },
            id: null,
            isUpdate: false,
        };
    },

    created() {
        console.log(window.location);
        const path = window.location.pathname;
        const pathes = path?.split("/").filter((p) => p !== "");
        console.log(pathes);
        if (pathes?.length >= 3 && pathes[2] !== "create") {
            this.id = pathes[2];
            this.isUpdate = true;

            http.get(`/api/page/sales/${this.id}`).then((response) => {
                const sale = response.data.data;
                console.log(sale);

                this.sale.id = sale.id;
                this.sale.is_main = sale.is_main;
                this.sale.show = sale.show;
                this.sale.preview_image = sale.preview_image;
                this.sale.title = sale.title;
                // Ahuet privodit iz bэкаa
                this.sale.text = sale.text;
                this.sale.image = sale.image;
                this.sale.itemId = sale.id;
                this.additionalFields = sale.items
                this.sale.seo_title = sale.seo_title,
                this.sale.seo_description = sale.seo_description,
                this.sale.seo_content = sale.seo_content
                this.additionalFields.forEach((element,index) => {
                    element.image.forEach((elementt, indexx) => {
                        this.additionalFields[index].image[indexx] = { instance:  elementt}
                        this.getBase64Image(elementt, index, indexx)
                    })
                });
                this.selectedProducts = sale.products;
            });
        }
        else{
           this.addField()
        }
    },
    methods: {
        getBase64Image(url, a, b) {
            this.getBase64Imagee(url,a, b, function (dataURL) {
                return url
            })
        },
        getBase64Imagee(urll, a, b, callback){
            const img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.onload = () => {
              const canvas = document.createElement("canvas");
              canvas.width = img.width;
              canvas.height = img.height;
              const ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0);
              const dataURL = canvas.toDataURL("image/png");
              console.log(dataURL)
              this.additionalFields[a].image[b].instance = dataURL
              callback(dataURL)
            }
            img.src = urll
        },
        getPreviewImage(url){
            const img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.onload = () => {
              const canvas = document.createElement("canvas");
              canvas.width = img.width;
              canvas.height = img.height;
              const ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0);
              const dataURL = canvas.toDataURL("image/png");
              console.log(dataURL)
              this.sale.preview_image = dataURL
            }
            img.src = url

        },
        setProducts(products) {
            this.selectedProducts = [...products];
        },

        deleteProduct(id) {
            console.log("ID", id);
            const foundIndex = this.selectedProducts.findIndex(
                (p) => p.id === id
            );
            if (foundIndex !== -1) {
                this.selectedProducts.splice(foundIndex, 1);
            }
        },
        addImagee(e){
            e.image.push({
                instance: ""
            })
        },
        addField() {
            const uuid = Math.random().toString(16).slice(2);
            const item = {
                uuid: uuid,
                text: "",
                image: [{
                    instance: ""
                }],
            };
            this.additionalFields.push(item);

            this.$nextTick(() => {
                document.getElementById(uuid).scrollIntoView();
            });
        },

        deleteAdditionalField(index, item) {
            this.isLoading = true;
            console.log(item);
            if (item?.id) {
                http.put(`/api/sale-items-change`, {
                    id_sale: parseInt(this.id),
                    id_item: parseInt(item.id),
                })
                    .then(() => {
                        this.isLoading = false;
                        this.additionalFields.splice(index, 1);
                    })
                    .catch(() => {
                        this.isLoading = false;
                    });
                return;
            }
            this.isLoading = false
            this.additionalFields.splice(index, 1);
        },

        mainImageHanlder(e) {
            console.log(e.target.files[0]);
            this.convertBase64(e.target.files[0]).then((result) => {
                this.sale.preview_image = result;
            });
        },

        initialImageHanlder(e) {
            this.convertBase64(e.target.files[0]).then((result) => {
                this.sale.image = result;
            });
        },

        additionalImageHanlder(e) {
            const parentIndex = e.target.parentNode.dataset.index
            const index = e.target.dataset.index;
            console.log(e.target);
            this.convertBase64(e.target.files[0]).then((result) => {
                this.additionalFields[parentIndex].image[index].instance = result;
            });
        },
        save() {

            if (this.id) {
                this.updateSale();
                return;
            }
            this.createSale();
        },
        createSale() {
          this.isLoading = true;

            this.sale.is_main = this.sale.is_main ? 1 : 0;
            this.sale.show = this.sale.show ? 1 : 0;
            const body = {
                ...this.sale,
                sales: [...this.additionalFields],
                products: [...this.selectedProducts],
            };

            http.post(`/api/sales-create`, body)
                .then((response) => {
                    console.log(response);
                    this.isLoading = false;
                    this.responseMessage.success = response.data.message;
                    setTimeout(() => {
                        window.location.href = "/admin/sales";
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 1000);
                })
                .catch((e) => {
                    this.isLoading = false;
                    this.responseMessage.error = e.response.data.message;
                    setTimeout(() => {
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 4000);
                });
        },
        updateSale() {
          this.isLoading = true;

            this.sale.is_main = this.sale.is_main ? 1 : 0;
            this.sale.show = this.sale.show ? 1 : 0;

            const body = {
                ...this.sale,
                sales: [...this.additionalFields],
                products: [...this.selectedProducts],
            };
            // body.sales.push(this.sale);
            http.put(`/api/sales-change/${this.id}`, body)
                .then((response) => {
                    console.log(response);
                    this.isLoading = false;
                    this.responseMessage.success = response.data.message;
                    setTimeout(() => {
                        // window.location.href = "/admin/sales";
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 1000);
                })
                .catch((e) => {
                    this.isLoading = false;
                    this.responseMessage.error = e.response.data.message;
                    setTimeout(() => {
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 4000);
                });
        },
        convertBase64(file) {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            return new Promise((resolve, reject) => {
                fileReader.onerror = () => {
                    fileReader.abort();
                    reject("Error when converting base64");
                };

                fileReader.onload = (e) => {
                    const result = e.target.result;
                    resolve(result);
                };
            });
        },
    },
};
