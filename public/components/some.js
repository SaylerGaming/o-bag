export default {
    template: `
        <div>
            <form @submit.prevent="sendFormData()">
            <div class="checkboxes">
                <div class="checkboxes-checkbox">
                    <h4> Показывать</h4>
                    <input  type="checkbox" name="is_constructor"   v-model="show">
                </div>
                <div class="checkboxes-checkbox">
                    <h4> На главной </h4>
                    <input type="checkbox" v-model="isMain">
                </div>
            </div>
            <editor       :init="{
        plugins: 'lists link image table code help wordcount'
      }"/>
                <h4> Картинка на главной </h4>
                <input type="file" @change="uploadMin" required ref="mainTitle">

                <h4> Название </h4>
                <div class="multiple__item" v-for="(item, index) in items" :key="index">
                    <p>Фотография</p>
                    <input type="file"  required  :ref="'uploadFile' + index" >
                    <h4>текст</h4>
                        <quill-editor style="max-height:300px; overflow: auto;" v-model="item.text" ref="quillEditor" :options="editorOption">
                </vue-quill-editor>
                </div>
                <p></p>
                <button class="btn btn-success" @click.prevent="items.push({file:null,text:'',})">Добавить новые поля</button>
                <input class="btn btn-primary" type="submit" value="Сохранить" style="margin-top: 50px">
            </form>
        </div>
        `,

    methods: {
        uploadMin(event) {
            var file = this.$refs.mainTitle.files[0];
            var reader = new FileReader();
            reader.onload = (e) => {
                this.mainImage = reader.result;
            };
            reader.readAsDataURL(file);
        },
        sendFormData() {
            console.log(this.$refs);
            var itemss = [];
            Object.values(this.$refs).forEach((element, index) => {
                console.log(element);
                if (element.length === 1) {
                    itemss.push(element.files[0]);
                } else {
                    itemss.push(element.files[0]);
                }
            });

            console.log(Object.keys(this.$refs).length);
            Object.keys(this.$refs).forEach((element, index) => {
                var reader = new FileReader();
                let file = null;
                switch (element) {
                    case "mainTitle":
                        file = this.$refs.mainTitle.files[0];
                        reader.onload = (e) => {
                            this.mainImage = reader.result;
                            if (
                                this.itemImages.length ===
                                Object.keys(this.$refs).length
                            ) {
                                this.mainMethod();
                            }
                        };
                        reader.readAsDataURL(file);
                        break;

                    default:
                        console.log(this.$refs[element][0].files[0]);
                        file = itemss[index];
                        reader.onload = (e) => {
                            this.itemImages.push(reader.result);
                            if (
                                this.itemImages.length ===
                                Object.keys(this.$refs).length - 1
                            ) {
                                this.mainMethod();
                            }
                        };
                        reader.readAsDataURL(file);
                        break;
                }
            });
        },
        mainMethod() {
            this.items.forEach((element, index) => {
                element.file = this.itemImages[index];
                this.wholeArray.push({
                    title: this.title,
                    show: this.show,
                    is_main: this.isMain,
                    preview_image: this.mainImage,
                    text: element.text,
                    image: element.file,
                });
            });
            let url = window.location.pathname.split("/").pop();
            if (url === "create") {
                axios
                    .post("https://api.obagofficial.kz/api/sales-create", {
                        sales: this.wholeArray,
                        products: [
                            {
                                id: 1,
                            },
                            {
                                id: 22,
                            },
                        ],
                    })
                    .then((res) => {
                        window.location.href = "/admin/sales";
                    });
            } else {
                let fulladress = window.location.pathname.split("/");
                fulladress.pop();
                let urlId = fulladress.pop();
                let sendUrl =
                    "https://api.obagofficial.kz/api/sales-change/" + urlId;
                axios
                    .put(sendUrl, {
                        sales: this.wholeArray,
                        products: [
                            {
                                id: 1,
                            },
                            {
                                id: 22,
                            },
                        ],
                        id: Number(urlId),
                    })
                    .then((res) => {
                        window.location.href = "/admin/sales";
                    });
            }
            this.wholeArray = [];
        },
    },
    data() {
        return {
            wholeArray: [],
            title: "",
            isMain: false,
            itemImages: [],
            mainImage: null,
            show: false,
            content: "Hellow contents",
            editorOption: {
                theme: "snow",
            },
            items: [
                {
                    file: null,
                    text: "",
                },
            ],
        };
    },
};
