import { http } from "/components/axios.js";

export default {
    name: "SearchProduct",
    template: `
        <div class="products-search-wrapper">
            <div class="form-group">
                <label    for="exampleInputEmail1">Продукты</label>
                <input v-model="term" @input="search"  type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div v-if="products && !isLoading" class="list-group">

                <a  v-for="product in products"
                    @click.prevent="selectProduct(product)"
                    href="#"
                    class="list-group-item"
                    :class="{'active':selectedProducts.find(p=>p.id === product.id)}">

                    {{product.title}}
                </a>

            </div>
            <div v-if="isLoading">
                Loading products...
            </div>
        </div>
    `,
    props: {
        items: {
            type: Array,
            default: () => [],
        },
    },
    data() {
        return {
            term: "",
            products: null,
            selectedProducts: [],
            isLoading: false,
        };
    },
    watch: {
        items: {
            handler() {
                this.selectedProducts = this.items;
            },
        },
    },
    // created() {
    //     if (this.items.length) {
    //         console.log("ITEMS");
    //         this.selectedProducts = [...this.items];
    //         console.log("ITEMS", this.selectedProducts);
    //     }
    // },
    methods: {
        search() {
            this.isLoading = true;

            http.get(`/api/products/search/${this.term}`)
                .then((response) => {
                    console.log(response.data);
                    this.isLoading = false;
                    this.products = response.data.products;
                })
                .catch(() => {
                    this.isLoading = false;
                });
        },

        selectProduct(product) {
            const foundIndex = this.selectedProducts.findIndex(
                (item) => item.id === product.id
            );

            if (foundIndex === -1) {
                this.selectedProducts.push({
                    id: product.id,
                    title: product.title,
                });
            } else {
                this.selectedProducts.splice(foundIndex, 1);
            }

            this.$emit("onSelectedItems", this.selectedProducts);
        },
    },
};
