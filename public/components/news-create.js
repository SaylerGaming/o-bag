import { http } from "/components/axios.js";
import SearchProduct from "/components/search-product.js";

export default {
    name: "SalesCreate",
    components: { SearchProduct },
    template: `
        <form @submit.prevent="save">
            <div class="row">
                <div class="col-md-3">
                    <input v-model="news.show"  class="form-check-input" type="checkbox" id="publishedCheckbox" value="option1">
                    <label class="form-check-label" for="publishedCheckbox">Показывать</label>
                </div>
            </div>

            <div class="form-group">
                <label    for="exampleInputEmail1">Загаловок</label>
                <input v-model="news.title"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>

            <div class="form-group">
                <label    for="exampleInputEmail1">Подзаголовок</label>
                <input v-model="news.subtitle"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>

            <hr/>
            <div class="form-group">
                <label    for="exampleInputEmail1">SEO title</label>
                <input v-model="news.seo_title"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div class="form-group">
                <label    for="exampleInputEmail1">SEO description</label>
                <input v-model="news.seo_description"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>
            <div class="form-group">
                <label    for="exampleInputEmail1">SEO content</label>
                <input v-model="news.seo_content"   type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
            </div>

            <hr/>

            <div class="additional-fields-wrapper">
                <div class="form-group">
                    <label for="exampleInputEmail1">Изображение новости</label>
                    <input @change="initialImageHanlder"  type="file" accept=".jpg, .jpeg, .png" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
                </div>

                <!-- Preview  Image start -->
                <div v-if="news.image" class="image-preview">
                    <img :src="news.image">
                </div>
                <!--  preview END -->complete_category_id
                <div class="form-group">
                    <label for="exampleInputEmail1">Описание товара</label>
                    <quill-editor v-model="news.text" required style="max-height:300px; overflow: auto;"  :options="editorOption">
                </div>
            </div>
                <p>Новые поля</p>
            <div class="additional-fields-wrapper"  v-for="(additionalField, index) in additionalFields" :key="additionalField.uuid" :id="additionalField.uuid">
                <button class="btn btn-danger btn-delete-float" @click.prevent="deleteAdditionalField(index, additionalField)" >
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
                <div  class="images-group" v-for="(imagee, indexx) in additionalField.image ">
                    <div class="form-group" :data-index="index">
                        <label for="exampleInputEmail1">Изображение</label>
                        <input @change="additionalImageHanlder" :data-index="indexx" type="file" accept=".jpg, .jpeg, .png" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите называние">
                    </div>
                    <!-- Preview  Image start -->
                    <div v-if="imagee" class="image-preview">
                        <img :src="imagee.instance">
                    </div>
                </div>
                    <button type="submit" class="btn btn-success" @click.prevent="addImagee(additionalField)">Добавить новую картинку</button>
                <!--  preview END -->

                <div class="form-group">
                    <label for="exampleInputEmail1">Описание</label>
                    <quill-editor v-model="additionalField.text" style="max-height:300px; overflow: auto;"  :options="editorOption">
                </div>
            </div>

            <hr/>

            <button type="submit" class="btn btn-success" @click.prevent="addField">Добавить поле</button>
            <button type="submit" class="btn " :class="[isLoading ? 'btn-warning'  : 'btn-primary']" :disabled="isLoading">Сохранить</button>
            <div v-if="responseMessage.error" class="alert alert-danger" role="alert">{{responseMessage.error}}</div>
            <div v-if="responseMessage.success" class="alert alert-success" role="alert">{{responseMessage.success}}</div>
        </form>
    `,

    data() {
        return {
            editorOption: {
                theme: "snow",
            },
            news: {
                title: "",
                subtitle: "",
                show: false,
                text: "",
                image: "",
                seo_title:'no title',
                seo_content:'no content',
                seo_description:'no description'
            },
            additionalFields: [],
            isLoading: false,
            responseMessage: {
                error: "",
                success: "",
            },
            id: null,
            isUpdate: false,
        };
    },

    created() {
        const path = window.location.pathname;
        const pathes = path?.split("/").filter((p) => p !== "");
        console.log(pathes);
        if (pathes?.length >= 3 && pathes[2] !== "create") {
            this.id = pathes[2];
            this.isUpdate = true;

            http.get(`/api/page/get-news/${this.id}`).then((response) => {
                const news = response.data.data;
                console.log(news);

                this.news.id = news.id;

                this.news.title = news.title;
                this.news.subtitle = news.subtitle;
                this.news.show = news.show;
                // Ahuet privodit iz bэкаa
                this.news.text = news.text;
                this.news.image = news.image;
                this.additionalFields = news.items;
                this.news.seo_title = news.seo_title,
                this.news.seo_description = news.seo_description,
                this.news.seo_content = news.seo_content
                this.additionalFields.forEach((element,index) => {
                    element.image.forEach((elementt, indexx) => {
                        this.additionalFields[index].image[indexx] = { instance:  elementt}
                        this.getBase64Image(elementt, index, indexx)
                    })
                });
                this.selectedProducts = news.products;
            });
        }
    },

    methods: {
        getBase64Image(url, a, b) {
            this.getBase64Imagee(url,a, b, function (dataURL) {
                return url
            })
        },
        getBase64Imagee(urll, a, b, callback){
            const img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.onload = () => {
              const canvas = document.createElement("canvas");
              canvas.width = img.width;
              canvas.height = img.height;
              const ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0);
              const dataURL = canvas.toDataURL("image/png");
              console.log(dataURL)
              this.additionalFields[a].image[b].instance = dataURL
              callback(dataURL)
            }
            img.src = urll
        },
        addImagee(e){
            e.image.push({
                instance: ""
            })
        },
        addField() {
            const uuid = Math.random().toString(16).slice(2);
            const item = {
                uuid: uuid,
                text: "",
                image: [{
                    instance: ""
                }],
            };
            this.additionalFields.push(item);

            this.$nextTick(() => {
                document.getElementById(uuid).scrollIntoView();
            });
        },
        deleteAdditionalField(index, item) {
            this.isLoading = true;
            console.log(item);
            if (item?.id) {
                http.put(`/api/news-items-change`, {
                    id_news: this.id,
                    id_item: parseInt(item.id),
                })
                    .then(() => {
                        this.isLoading = false;
                        this.additionalFields.splice(index, 1);
                    })
                    .catch(() => {
                        this.isLoading = false;
                    });
                return;
            }
            this.isLoading = false
            this.additionalFields.splice(index, 1);
        },

        initialImageHanlder(e) {
            this.convertBase64(e.target.files[0]).then((result) => {
                this.news.image = result;
            });
        },

        additionalImageHanlder(e) {
            const parentIndex = e.target.parentNode.dataset.index
            const index = e.target.dataset.index;
            console.log(e.target);
            this.convertBase64(e.target.files[0]).then((result) => {
                this.additionalFields[parentIndex].image[index].instance = result;
            });
        },
        save() {
            this.isLoading = true;

            if (this.id) {
                this.updateSale();
                return;
            }
            this.createSale();
        },
        createSale() {
            // const mapFieldsToServer = this.additionalFields.filter(()=>index !== 0);
            this.news.show = this.news.show ? 1 : 0;

            const body = {
                title: this.news.title,
                subtitle: this.news.subtitle,
                show: this.news.show,
                text: this.news.text,
                image: this.news.image,
                news: [],
                seo_title:this.news.seo_title,
                seo_description:this.news.seo_title,
                seo_content:this.news.seo_title,
            };

            if (this.additionalFields.length > 0) {
                console.log('crinnge');
                body.news = [...this.additionalFields];
            }

            http.post(`/api/news-create`, body)
                .then((response) => {
                    console.log(response);
                    this.isLoading = false;
                    this.responseMessage.success = response.data.message;
                    setTimeout(() => {
                        window.location.href = "/admin/new-sales";
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 1000);
                })
                .catch((e) => {
                    this.isLoading = false;
                    this.responseMessage.error = e.response.data.message;

                    setTimeout(() => {
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                      window.location.href = "/admin/news";

                    }, 4000);
                });
        },
        updateSale() {
            this.news.show = this.news.show ? 1 : 0;

            const body = {
                title: this.news.title,
                subtitle: this.news.subtitle,
                show: this.news.show,
                image: this.news.image,
                text: this.news.text,
                seo_title:this.news.seo_title,
                seo_description:this.news.seo_title,
                seo_content:this.news.seo_title,
            };

            if (this.additionalFields.length) {
                body.news = [...this.additionalFields];
            }

            http.put(`/api/news-change/${this.id}`, body)
                .then((response) => {
                    console.log(response);
                    this.isLoading = false;
                    this.responseMessage.success = response.data.message;
                    setTimeout(() => {
                        // window.location.href = "/admin/sales";
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 1000);
                })
                .catch((e) => {
                    this.isLoading = false;
                    this.responseMessage.error = e.response.data.message;
                    setTimeout(() => {
                        this.responseMessage = {
                            error: "",
                            success: "",
                        };
                    }, 4000);
                });
        },
        convertBase64(file) {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            return new Promise((resolve, reject) => {
                fileReader.onerror = () => {
                    fileReader.abort();
                    reject("Error when converting base64");
                };

                fileReader.onload = (e) => {
                    const result = e.target.result;
                    resolve(result);
                };
            });
        },
    },
};

