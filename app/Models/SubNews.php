<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubNews extends Model
{
    use HasFactory;

    protected $fillable = [
        'new_sales_id',
        'text',
        'image'
    ];

    protected $casts = [
        'image' => 'array'
    ];

    public function news(): BelongsTo {
        return $this->belongsTo(NewSales::class, 'new_sales_id');
    }
}
