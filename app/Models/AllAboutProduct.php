<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class AllAboutProduct extends Model
{
    use HasFactory, Translatable;

    protected $fillable = [
        'number',
        'time'
    ];

    protected $translatable = [
        'text',
    ];
}
