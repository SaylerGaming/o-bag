<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Translatable;

class NewSales extends Model
{
    use HasFactory, Translatable;

    protected $fillable = [
        'title',
        'text',
        'subtitle',
        'image',
        'is_main',
        'show',
        'seo_title',
        'seo_description',
        'seo_content',
    ];

    protected $translatable = [
        'title',
        'text',
        'subtitle'
    ];

    public function getStorageLinkAttribute($img_str) {
        return Storage::disk(config('voyager.storage.disk'))->url($img_str);
    }

    public function getPhotosArrayAttribute($arr_str) {
        $arr = json_decode($arr_str);
        $length = count((array)$arr);
        for ($i = 0; $i < $length; $i++) {
            $arr[$i] = $this->getStorageLinkAttribute($arr[$i]);
        }
        return $arr;
    }
}
