<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubSales extends Model
{
    use HasFactory;

    protected $fillable = [
        'sales_id',
        'text',
        'image'
    ];

    protected $casts = [
        'image' => 'array'
    ];

    public function sales(): BelongsTo {
        return $this->belongsTo(Sale::class, 'sales_id');
    }
}
