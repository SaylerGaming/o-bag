<?php

namespace App\Jobs;

use App\Mail\DeliveryMail;
use App\Mail\OrderMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MailSendJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $cart;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mail, $cart)
    {
        //
        $this->mail = $mail;
        $this->cart = $cart;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Illuminate\Support\Facades\Mail::to($this->mail)
            ->send(new OrderMail($this->cart))
        ;

        \Illuminate\Support\Facades\Mail::to($this->mail)
            ->send(new DeliveryMail($this->cart))
        ;
    }
}
