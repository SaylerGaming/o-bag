<?php

namespace App\Jobs;

use App\Models\CompleteCategory;
use App\Models\ElementProduct;
use App\Models\FilterCategory;
use App\Models\FilterElement;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 5000;
    protected $rows;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($rows)
    {
        $this->rows = $rows;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rows = $this->rows;
        try {
//            DB::beginTransaction();

            foreach ($rows as $row) {
                if ($row[0] == 'Артикул' or $row[0] == null) {

                } else {
//                    $check = Product::query()
//                                    ->where('code', $row[0])
//                                    ->exists()
//                    ;
//                    if ($row[18] != null) {
//                        $subcategory = Subcategory::query()
//                                                  ->where('title', $row[18])
//                                                  ->first()
//                        ;;
//                        if (!empty($subcategory)) {
//                            $subcategory = $subcategory->first()->id;
//                        }
//                    }
//                    if ($row[19] != null) {
//                        $complete = CompleteCategory::query()
//                                                    ->where('title', $row[19])
//                        ;
//                        if ($complete->exists()) {
//                            $complete = $complete->first()->id;
//                        } else {
//                            $complete = CompleteCategory::query()
//                                                        ->create(
//                                                            [
//                                                                'title'          => $row[19],
//                                                                'image'          => 'NULL',
//                                                                'subcategory_id' => empty($subcategory->id) ? null : $subcategory->id,
//                                                                'type'           => empty($row[6]) ? "koshelki" : $row[6],
//                                                            ]
//                                                        )->id;
//                        }
//                    } else {
//                        $complete = null;
//                    }
//                    if ($row[3] != null) {
//                        $images = Storage::disk('public')
//                                         ->allFiles('products/' . $row[3] . '/')
//                        ;
//                        if (isset($images[1])) {
//                            $consImage = json_encode([$images[1]]);
//                        } elseif (isset($images[0])) {
//                            $consImage = json_encode([$images[0]]);
//                        } else {
//                            $consImage = null;
//                        }
//                        $images = json_encode(array_values($images));
//
//                    }
                    $characteristics = '<p>' . 'Артикул: ' . $row[0] . '</p>';
                    if ($row[2] != null) {
                        $characteristics .= '<p>'   . $row[2] . '</p>';
                    }
                    if ($row[5] != null) {
                        $characteristics .= '<p>' . $row[4] . '</p>';
                    }

                    if ($row[6] != null) {
                        $characteristics .= '<p>' . 'Тип: ' . $row[6] . '</p>';
                    }
                    if ($row[7] != null) {
                        $characteristics .= '<p>'  . $row[7] . '</p>';
                    }
                    if ($row[8] != null) {
                        $characteristics .= '<p>' . $row[8] . '</p>';
                    }
                    if ($row[9] != null) {
                        $characteristics .= '<p>' . $row[9] . '</p>';
                    }
                    if ($row[10] != null) {
                        $characteristics .= '<p>'   . $row[10] . '</p>';
                    }
                    if ($row[11] != null) {
                        $characteristics .= '<p>'  . $row[11] . '</p>';
                    }
                    if ($row[12] != null) {
                        $characteristics .= '<p>'  . $row[12] . '</p>';
                    }
                    if ($row[13] != null) {
                        $characteristics .= '<p>' . $row[13] . '</p>';
                    }
                    if ($row[14] != null) {
                        $characteristics .= '<p>'  . $row[14] . '</p>';
                    }
                    if ($row[15] != null) {
                        $characteristics .= '<p>'   . $row[15] . '</p>';
                    }
                    if ($row[16] != null) {
                        $characteristics .= '<p>'   . $row[16] . '</p>';
                    }
                    if ($row[17] != null) {
                        $description = '<p>' . $row[22] . '</p>';
                    }

                    if ($row[23] == 'Да') {
                        $isConstructor = true;
                    } else {
                        $isConstructor = false;
                    }

                    if ($row[21] == null) {
                        $row[21] = 0;
                    }

                    $product = Product::query()
                                      ->updateOrCreate(
                                          [
                                              'code' => $row[0],
                                          ],
                                          [
//                                              'code'              => $row[0],
//                                              'title'             => $row[1],
                                              'price'             => $row[17],
                                              'characteristics'   => $characteristics,
                                              'description'       => $description ?? null,
//                                              'image'             => $images,
//                                              'video'             => $row[5],
//                                              'remainder'         => $row[21] ?? null,
//                                              'subcategory_id'    => $subcategory->id ?? null,
//                                              'is_constructor'    => $isConstructor,
//                                              'constructor_image' => $consImage ?? null,
//                                                           'seo_title'       => $row[26],
//                                                           'seo_description' => $row[27],
                                              //'seo_content'     => $row[28],
                                          ]
                                      )
                    ;
//                    dd($product, $consImage);
//                    $product->completeCategories()
//                            ->sync($complete)
//                    ;
//                                            dd($rows);
//                                            if ($row[21] != null) {
//                                                $data = explode(',', $row[21]);
//                                                foreach ($data as $item) {
//                                                    $item            = trim($item);
//                                                    $completeProduct = Product::query()
//                                                                              ->where('title', $item)
//                                                                              ->first()
//                                                    ;
//                                                    CompleteProduct::query()
//                                                                   ->create([
//                                                                                'complete_id' => $completeProduct->id,
//                                                                                'product_id'  => $product->id,
//                                                                            ])
//                                                    ;
//                                                }
//
//                                            }


//                    if ($row[7] != null) {
//                        $this->filterCreate('Назначение', $row[7], $product->id);
//                    }
//                    if ($row[8] != null) {
//                        $this->filterCreate('Материал', $row[8], $product->id);
//                    }
//
//                    if ($row[9] != null) {
//                        $this->filterCreate('Цвет', $row[9], $product->id);
//                    }
//
//                    if ($row[10] != null) {
//                        $this->filterCreate('Застежка', $row[10], $product->id);
//                    }
//
//                    if ($row[11] != null) {
//                        $this->filterCreate('Отделение для монет', $row[11], $product->id);
//                    }
//
//                    if ($row[12] != null) {
//                        $this->filterCreate('Отделения для карт/визиток', $row[12], $product->id);
//                    }
//
//                    if ($row[13] != null) {
//                        $this->filterCreate('Страна производства', $row[13], $product->id);
//                    }
//
//                    if ($row[14] != null) {
//                        $this->filterCreate('Размеры', $row[14], $product->id);
//                    }

                }
            }
        } catch
        (\Exception $e) {
//            DB::rollBack();
            dd($e);
        }
        DB::commit();

        return [
            'message' => 'done',
        ];
    }

    public function filterCreate($title, $row, $product_id)
    {
        $filterCategories = FilterCategory::query()
                                          ->where('title', $title)
                                          ->first()
        ;

        if ($filterCategories == null) {
            $filterCategories = FilterCategory::query()
                                              ->create([
                                                           'title' => $title,
                                                       ])
            ;
        }

        $element = FilterElement::query()
                                ->where([
                                            'category_id' => $filterCategories->id,
                                            'title'       => $row,
                                        ])
                                ->first()
        ;

        if ($element == null) {
            if ($row != null) {
                $element = FilterElement::query()
                                        ->create([
                                                     'category_id' => $filterCategories->id,
                                                     'title'       => $row,
                                                 ])
                ;
            }
        }
        if ($row != null) {
            ElementProduct::query()
                          ->create([
                                       'element_id' => $element->id,
                                       'product_id' => $product_id,
                                   ])
            ;
        }
    }

}
