<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsCreationRequest;
use App\Http\Requests\NewsItemsChangeRequest;
use App\Http\Requests\SaleItemsChangeRequest;
use App\Http\Requests\SalesCreationRequest;
use App\Http\Resources\NewsResource;
use App\Http\Resources\SaleResource;
use App\Models\CompleteCategory;
use App\Models\CompleteProduct;
use App\Models\NewSales;
use App\Models\Product;
use App\Models\SubNews;
use App\Models\SubSales;
use Illuminate\Http\Request;

use App\Models\Sale;
use App\Models\City;
use App\Models\Banner;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function PHPUnit\Framework\isEmpty;

class PageController extends Controller
{
    public function sales()
    {
        $news_items = NewSales::query()
            ->select(
                'id',
                'image',
                'text',
                'title',
                'subtitle',
                'badge',
                'created_at',
                'seo_title', 'seo_description', 'seo_content'

            )
            ->orderBy('created_at', 'desc')
            ->where('show', 1)
            ->get()
            ->translate(\request()->header('Accept-Language'));


        foreach ($news_items as $news) {
            if ($news->translations->isEmpty()) {
                $news = $news->translate('ru');
            }
            $news->slug = Str::slug($news->title);

            $image = $news->image; //your base64 encoded data
            $image = str_contains($image, 'base64') ? $this->makeImage($image) : $image;
        }

        return response()->json([
            'news' => NewsResource::collection($news_items),
        ]);
    }

    public function salesById(int $id)
    {
        $article = NewSales::query()
            ->find($id)//
            ->translate(\request()->header('Accept-Language'));

        if ($article == null) {
            return response()->json([
                'message' => 'Страница не найдена',
            ], 404);
        }
        //$article->image = env('APP_URL') . '/storage/' . $article->image;

        $article->save();

        return new NewsResource($article);
    }

    public function history()
    {
        $content = [
            'title' => setting('history.title'),
            'image' => env('APP_URL') . '/storage/' . setting('history.image'),
            'text' => setting('history.text'),
        ];

        return response()->json([
            'content' => $content,
        ]);
    }

    public function mission()
    {
        $content = [
            'title' => setting('mission.title'),
            'image' => env('APP_URL') . '/storage/' . setting('mission.image'),
            'text' => setting('mission.text'),
        ];

        return response()->json([
            'content' => $content,
        ]);
    }

    public function brandInfo()
    {
        $content = [
            'title' => setting('brand-info.title'),
            'text' => setting('brand-info.text'),
        ];

        return response()->json([
            'content' => $content,
        ]);
    }

    public function makeImage($image)
    {
        $ext = explode(';base64', $image);
        $ext = explode('/', $ext[0]);
        $ext = $ext[1];
        $replace = substr($image, 0, strpos($image, ',') + 1);

        $image = str_replace($replace, '', $image);

        $image = str_replace(' ', '+', $image);

        $imageName = Str::random(10) . '.' . $ext;
        Storage::disk('public')->put($imageName, base64_decode($image));
        return Storage::disk(config('voyager.storage.disk'))->url($imageName);
    }

    public function getNews()
    {
        $news_items = Sale::query()
            ->select(
                'id',
                'is_main',
                'show',
                'title',
                'image',
                'text',
                'preview_image',
                'created_at', 'seo_title', 'seo_description', 'seo_content'
            )
            ->orderBy('created_at', 'desc')
            ->where('show', 1)
            ->get()
            ->translate(\request()->header('Accept-Language'));
        foreach ($news_items as $news) {
//            if ($news->translations->isEmpty()) {
//                $news = $news->translate('ru');
//            }

            $image = $news->image; //your base64 encoded data
            $news->image = str_contains($image, 'base64') ? $this->makeImage($image) : $image;

            if ($news->preview_image != null) {

                $image_pre = $news->preview_image;
                $news->preview_image = str_contains($image_pre, 'base64') ? $this->makeImage($image_pre) : $image_pre;
            }
        }

        return response()->json([
            'sales' => SaleResource::collection($news_items)
        ]);
    }

    public function new(int $id)
    {
        $article = Sale::query()
            ->find($id);
//        if (!empty($article->preview_image)) {
//            $article->preview_image = env('APP_URL') . '/storage/' . $article->preview_image;
//        }
        if ($article == null) {
            return response()->json([
                'message' => 'Страница не найдена',
            ], 404);
        }

        //$article->image = env('APP_URL') . '/storage/' . $article->image;


        $productIds = DB::table('sale_products')
            ->where('sale_id', $id)
            ->pluck('product_id')
            ->toArray();;
        $products = Product::query()
            ->select(
                'id',
                'title',
                'price',
                'new_price',
                'image',
            )
            ->whereIn('id', $productIds)
            ->get();
//        foreach ($products as $product) {
//            if (json_decode($product->image) != null) {
//                $product->image = Storage::disk(config('voyager.storage.disk'))->url(json_decode($product->image)[0]);
//            } else {
//                $product->image = Storage::disk(config('voyager.storage.disk'))->url($product->image);
//            }
//            $product->save();
//        }
        $article->products()->sync($products);
        $article->save();

        return new SaleResource($article);
    }

    public function getCities()
    {
        $cities = City::with('fillials')
            ->get();

        return response()->json([
            'cities' => $cities,
        ]);
    }

    public function getBanners()
    {
        return response()->json(['banners' => Banner::all()]);
    }

    public function getCompleteProducts()
    {
        $data = [];
        $completeProducts = CompleteProduct::all();
        foreach ($completeProducts as $completeProduct) {
            $category = CompleteCategory::query()
                ->find($completeProduct->complete_id);

            $productIds = CompleteProduct::query()
                ->where('complete_id', $completeProduct->complete_id)
                ->pluck('product_id');

            $category->products = Product::query()
                ->whereIn('id', $productIds)
                ->get();
            $data[] = $category;
        }

        return $data;
    }

    public function salesCreate(SalesCreationRequest $request)
    {
        $data = $request->validated();

        $saleMain = Sale::create([
            'title' => $data['title'],
            'show' => $data['show'],
            'is_main' => $data['is_main'],
            'preview_image' => $this->makeImage($data['preview_image']),
            'text' => $data['text'],
            'image' => $this->makeImage($data['image']),
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'seo_content' => $request->seo_content,
        ]);
        $product_ids = Product::all()->whereIn('id', array_column($data['products'], 'id'))->pluck('id');
        $saleMain->products()->sync($product_ids);

        $images_arr = [];

        foreach ($data['sales'] as $sale) {
            foreach ($sale['image'] as $img) {
                array_push($images_arr, $this->makeImage($img['instance']));
            }
            $created_sale = SubSales::create([
                'sales_id' => $saleMain->id,
                'text' => $sale['text'],
                'image' => $images_arr,
            ]);
            $images_arr = [];
        }

        return response()->json(['message' => 'Акции созданы!', 'data' => $saleMain]);
    }

    public function newsCreate(NewsCreationRequest $request)
    {
        $data = $request->validated();

        $news = NewSales::create([
            'title' => $data['title'],
            'show' => $data['show'],
            'subtitle' => $data['subtitle'],
            'text' => $data['text'],
            'image' => $this->makeImage($data['image']),
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'seo_content' => $request->seo_content,
        ]);

        $images_arr = [];

        foreach ($data['news'] as $new) {
            foreach ($new['image'] as $img) {
                array_push($images_arr, $this->makeImage($img['instance']));
            }
            SubNews::create([
                'new_sales_id' => $news->id,
                'text' => $new['text'],
                'image' => $images_arr,
            ]);
            $images_arr = [];
        }

        return response()->json(
            ['message' => 'Новости созданы!', 'data' => $news]);
    }

    public function salesDelete($id)
    {
        $sale = Sale::find($id);
        $sale->delete();
        return response()->json(['message' => 'Акция удалена!']);
    }

    public function newsDelete($id)
    {
        $news = NewSales::find($id);
        $news->delete();
        return response()->json(['message' => 'Новость удалена!']);
    }

    public function salesChange(SalesCreationRequest $request, $id)
    {
        $data = $request->validated();
        $sale = Sale::find($id);
        $sale->update([
            'title' => $data['title'],
            'show' => $data['show'],
            'is_main' => $data['is_main'],
            'preview_image' => str_contains($data['preview_image'], 'base64') ? $this->makeImage($data['preview_image']) : $data['preview_image'],
            'text' => $data['text'],
            'image' => str_contains($data['image'], 'base64') ? $this->makeImage($data['image']) : $data['image'],
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'seo_content' => $request->seo_content,
        ]);

        $product_ids = Product::all()->whereIn('id', array_column($data['products'], 'id'))->pluck('id');
        $sale->products()->sync($product_ids);

        $images_arr = [];

        foreach ($data['sales'] as $sale) {
            foreach ($sale['image'] as $img) {
                array_push($images_arr, $this->makeImage($img['instance']));
            }
            if (empty($sale['id'])) {
                SubSales::create([
                    'sales_id' => $id,
                    'text' => $sale['text'],
                    'image' => $images_arr,
                ]);
            } else {
                $subsale = SubSales::find($sale['id']);
                $subsale->text = $sale['text'];
                //$subsale->image = str_contains($data['image'], 'base64') ? $this->makeImage($data['image']) : $data['image'];
                $subsale->image = $images_arr;
                $subsale->save();
            }
            $images_arr = [];
        }
        return response()->json(['message' => 'Акция изменена!']);
    }

    public function newsChange(NewsCreationRequest $request, $id)
    {
        $data = $request->validated();
        $news = NewSales::find($id);
        $news->update([
            'title' => $data['title'],
            'show' => $data['show'],
            'subtitle' => $data['subtitle'],
            'text' => $data['text'],
            'image' => str_contains($data['image'], 'base64') ? $this->makeImage($data['image']) : $data['image'],
            'seo_title' => $request->seo_title,
            'seo_description' => $request->seo_description,
            'seo_content' => $request->seo_content,
        ]);

        $images_arr = [];

        foreach ($data['news'] as $new) {
            foreach ($new['image'] as $img) {
                array_push($images_arr, $this->makeImage($img['instance']));
            }
            if (empty($new['id'])) {
                SubNews::create([
                    'new_sales_id' => $id,
                    'text' => $new['text'],
                    'image' => $images_arr,
                ]);
            } else {
                $subnew = SubNews::find($new['id']);
                $subnew->text = $new['text'];
                //$subnew->image = str_contains($data['image'], 'base64') ? $this->makeImage($data['image']) : $data['image'];
                $subnew->image = $images_arr;
                $subnew->save();
            }
            $images_arr = [];
        }

        return response()->json(['message' => 'Новость изменена!']);
    }

    public function saleItemsChange(SaleItemsChangeRequest $request)
    {
        $data = $request->validated();
        $item = SubSales::where('sales_id', $data['id_sale'])->where('id', $data['id_item'])->first();
        $item->delete();
        return response()->json(['message' => 'Блок данной акции удален!']);
    }

    public function newsItemsChange(NewsItemsChangeRequest $request)
    {
        $data = $request->validated();
        $item = SubNews::where('new_sales_id', $data['id_news'])->where('id', $data['id_item'])->first();
        $item->delete();
        return response()->json(['message' => 'Блок данной новости удален!']);
    }

    public function fixProductImageLinks(Request $request)
    {
        $products = Product::all();
        foreach ($products as $product) {
            $pos = strpos($product->image, 'products');
            $product->image = substr($product->image, $pos);
            $product->save();
        }
    }

    public function getall()
    {
        $products = $this->getAllFunc(Product::select('id', 'title')->get());

        return response($products, 200);
    }

    public function getAllNews()
    {
        $news = $this->getAllFunc(NewSales::select('id', 'title')->get());

        return response($news, 200);
    }

    public function getAllSale()
    {
        $sales = $this->getAllFunc(Sale::select('id', 'title')->get());

        return response($sales, 200);
    }

    private function getAllFunc($models)
    {
        foreach ($models as $model) {
            $model->slug = Str::slug($model->title);
            unset($model->title);
        }

        return $models;

    }
}
