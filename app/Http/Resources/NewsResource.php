<?php

namespace App\Http\Resources;

use App\Models\NewSales;
use App\Models\Sale;
use App\Models\SubNews;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'title'      => $this->title,
            'text' => $this->text,
            'image' => $this->image,
            'show' => $this->show == 1,
            'items' => ItemsResource::collection(SubNews::all()->where('new_sales_id', $this->id)),
            'created_at' => $this->created_at,
            'subtitle' => $this->subtitle,
            'seo_content' => $this->seo_content,
            'seo_description' => $this->seo_description,
            'seo_title' => $this->seo_title,
        ];
    }
}
