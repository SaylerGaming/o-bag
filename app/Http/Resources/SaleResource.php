<?php

namespace App\Http\Resources;

use App\Http\Resources\ProductResource;
use App\Models\Sale;
use App\Models\SubSales;
use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'title'      => $this->title,
            'text' => $this->text,
            'image' => $this->image,
            'items' => ItemsResource::collection(SubSales::all()->where('sales_id', $this->id)),
            'is_main'    => $this->is_main,
            'show' => $this->show == 1,
            'created_at' => $this->created_at,
            'preview_image' => $this->preview_image,
            'products'   => ProductResource::collection($this->products),
            'seo_title' => $this->seo_title,
            'seo_description' => $this->seo_description,
            'seo_content' => $this->seo_content,
        ];
    }


}
