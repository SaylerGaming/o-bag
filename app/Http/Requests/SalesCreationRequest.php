<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'show' => 'boolean',
            'is_main' => 'boolean' ,
            'preview_image' => 'string',
            'text' => 'string',
            'image' => 'string',

            'sales' => 'present|array',
            'sales.*.id' => 'numeric',
            'sales.*.text' => 'string',
            'sales.*.image' => 'present|array',
            'sales.*.image.*.instance' => 'string',

            'products' => 'array',
            'products.*.id' => 'numeric'
        ];
    }
}
