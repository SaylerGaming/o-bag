<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'subtitle' => 'string',
            'show' => 'boolean',
            'text' => 'string',
            'image' => 'string',

            'news' => 'array',
            'news.*.id' => 'numeric',
            'news.*.text' => 'string',
            'news.*.image' => 'present|array',
            'news.*.image.*.instance' => 'string'
        ];
    }
}
