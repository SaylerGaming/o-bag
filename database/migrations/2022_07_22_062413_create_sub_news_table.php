<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_news', function (Blueprint $table) {
            $table->id();
            $table->foreignId('new_sales_id')
                ->constrained('new_sales')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->longText('text');
            $table->longText('image');
            $table->timestamps();
        });

        Schema::table('sub_sales', function (Blueprint $table) {
            $table->longText('image')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_news');
    }
}
