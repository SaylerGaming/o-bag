@extends('voyager::master')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/quill@1.3.6/dist/quill.core.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/quill@1.3.6/dist/quill.snow.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/quill@1.3.6/dist/quill.bubble.css">
<style>
    .site-footer-right {
        display: none;
    }

    #app {
        /* overflow: scroll; */
    }
</style>
@section('content')
    <div id="app">

        <div class="container">

            <h1 v-if="!isUpdate" class="main_title"> Создание акции</h1>
            <h1 v-else class="main_title"> Обновление акции</h1>
            <sales-create />

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/quill@1.3.6/dist/quill.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2.7.0/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-quill-editor@3.0.6/dist/vue-quill-editor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/vue-virtual-scroller@1.0.10/dist/vue-virtual-scroller.umd.min.js"></script> --}}
    <script type="module">
        import SalesCreate from '/components/sales-create.js'
        import {
            http
        } from "/components/axios.js";
        Vue.use(VueQuillEditor);
        Vue.config.devtools = true;
        var app = new Vue({
            el: '#app',
            components: {
                SalesCreate
            },
            data() {
                return {
                    isUpdate: false,

                }
            },
            created() {
                const path = window.location.pathname.split("/").pop()
                if (path === "edit") {
                    this.isUpdate = true;
                }
            },
        })
    </script>
@endsection
<style>
    html,
    body {
        scroll-behavior: smooth;
    }

    .additional-fields-wrapper {
        position: relative;
    }


    .products-search-wrapper .list-group {
        max-height: 500px;
        overflow: auto;
        max-width: 100%;
    }

    .image-preview {
        position: relative;

    }

    .image-preview img {
        width: 100%;
        max-height: 200px;
        object-fit: cover;
    }

    .included-products {
        display: flex;
        max-width: 100%;
        flex-wrap: wrap;
    }

    .included-products span {
        display: inline-block;
        margin: 0 8px 8px;
    }

    .btn-delete-float {
        position: absolute;
        top: 10px;
        right: 5px;
    }

    .create-form {
        max-width: 500px;
        width: 100%;
    }

    .create-form p {
        margin: 0
    }

    .main_title {
        margin: 30px;
    }

    .checkboxes {
        display: flex;
        justify-content: space-around;
    }

    .checkboxes-checkbox {
        text-align: center
    }

    .checkboxes-checkbox p {
        margin: 0
    }

    .checkboxes-checkbox input {
        width: 40px;
        height: 30px;
    }

    .create-form {
        display: flex;
        justify-content: center;
        text-align: left;
        max-width: 500px;
        width: 100%;
        margin: auto;
    }

    .create-form form {
        width: 100%;
    }

    .create-form form input {
        width: 100%
    }

    .create-form form button {
        margin: auto;
        margin-top: 20px;
        width: 100%;
    }

    .btn-add-question {
        position: fixed;
        top: 58%;
        right: 2%;
        border-radius: 100%;
        z-index: 999;
    }

    .create-form .input-group {
        /* display: flex; */
    }

    .delete-start {
        transform: translateX(-1000px) transition:2s
    }
</style>
